# frozen_string_literal: true

require 'rspec'
require 'csv'
require_relative '../model/slcsp'

describe 'zip codes' do
  it 'should return zip codes from csv' do
    allow(CSV).to receive(:foreach)
      .and_return([%w[zipcode rate],
                   ['12345', nil],
                   ['12346', nil]])
    slcsp = Model::Slcsp.new('ignore me')

    zips = []
    slcsp.foreach_zip { |zip| zips << zip }

    expect(zips).to eq %w[12345 12346]
  end
end
