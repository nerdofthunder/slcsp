# frozen_string_literal: true

require 'rspec'
require_relative '../model/plan'

describe 'Find Rate' do
  it 'should find the rate of a plan corresponding to the search criteria' do
    allow(CSV).to receive(:foreach)
      .and_return([%w[plan_id state metal_level rate rate_area],
                   %w[74449NR9870320 GA Silver 900.62 7],
                   %w[74449NR9870320 GA Silver 298.62 8],
                   %w[74449NR9870320 NY Silver 298.62 7],
                   %w[74449NR9870320 GA Gold 298.62 7],
                   %w[74449NR9870320 GA Silver 298.62 7]])
    plan = Model::Plan.new('ignore me')
    rates = plan.find_and_sort_rates(state: 'GA', rate_area: '7')

    expect(rates).to eq([298.62, 900.62])
  end

  it 'should sort the rates of corresponding plans' do
    allow(CSV).to receive(:foreach)
      .and_return([%w[plan_id state metal_level rate rate_area],
                   %w[74449NR9870320 GA Silver 900.62 7],
                   %w[74449NR9870320 GA Silver 22.00 7],
                   %w[74449NR9870320 GA Silver 98.26 7],
                   %w[74449NR9870320 GA Silver 298.62 7],
                   %w[74449NR9870320 GA Silver 298.61 7]])
    plan = Model::Plan.new('ignore me')
    rates = plan.find_and_sort_rates(state: 'GA', rate_area: '7')

    expect(rates).to eq([22.00, 98.26, 298.61, 298.62, 900.62])
  end
end

describe 'get second lowest rate' do
  it 'should return the second lowest rate' do
    allow(CSV).to receive(:foreach)
      .and_return([%w[plan_id state metal_level rate rate_area],
                   %w[74449NR9870320 GA Silver 900.62 7],
                   %w[74449NR9870320 GA Silver 298.62 8],
                   %w[74449NR9870320 NY Silver 298.62 7],
                   %w[74449NR9870320 GA Gold 298.62 7],
                   %w[74449NR9870320 GA Silver 298.62 7]])
    plan = Model::Plan.new('ignore me')
    second_lowest_cost_rate = plan.get_second_lowest_rate(state: 'GA', rate_area: '7')

    expect(second_lowest_cost_rate).to eq(900.62)
  end
end
