# frozen_string_literal: true

require 'rspec'
require 'csv'
require_relative '../model/zip'

describe 'search by zip' do
  it 'should search for zip code and return it' do
    allow(CSV).to receive(:foreach)
      .and_return(
        [%w[zipcode state county_code name rate_area],
         %w[89406 NV 32001 Churchill 4],
         %w[89407 NV 32001 Churchill 4]]
      )
    zip = Model::Zip.new("doesn't matter")

    zip_map = zip.search_by_zip('89406')

    expect(zip_map).to eq(state: 'NV', rate_area: '4')
  end

  it 'should search for zip code and return nil if more than one rate area match' do
    allow(CSV).to receive(:foreach)
      .and_return(
        [%w[zipcode state county_code name rate_area],
         %w[89406 NV 32001 Churchill 4],
         %w[89406 NV 32001 Churchill 5]]
      )
    zip = Model::Zip.new("doesn't matter")

    zip_map = zip.search_by_zip('89406')

    expect(zip_map).to eq nil
  end
end
