# Calculate second lowest cost silver plan (SLCSP)

## Problem

You have been asked to determine the second lowest cost silver plan (SLCSP) for
a group of ZIP Codes.

## Task

You have been given a CSV file, `slcsp.csv`, which contains the ZIP Codes in the
first column. Fill in the second column with the rate (see below) of the
corresponding SLCSP. Your answer is the modified CSV file, which the program
should __emit on stdout__, plus any source code used.

Write your code in your best programming language.

### Expected output

The order of the rows in your answer as emitted on stdout must stay the same as how they
appeared in the original `slcsp.csv`. The first row should be the column headers: `zipcode,rate`
The remaining lines should output unquoted values with two digits after the decimal
place of the rates, for example: `64148,245.20`.

It may not be possible to determine a SLCSP for every ZIP Code given. Check for cases
where a definitive answer cannot be found and leave those cells blank in the output CSV (no
quotes or zeroes or other text). For example, `40813,`

## Additional information

The SLCSP is the so-called "benchmark" health plan in a particular area. It is
used to compute the tax credit that qualifying individuals and families receive
on the marketplace. It is the second lowest rate for a silver plan in the rate area.

For example, if a rate area had silver plans with rates of `[197.3, 197.3,
201.1, 305.4, 306.7, 411.24]`, the SLCSP for that rate area would be `201.1`,
since it is the second lowest rate in that rate area.

A plan has a "metal level", which can be either Bronze, Silver, Gold, Platinum,
or Catastrophic. The metal level is indicative of the level of coverage the plan
provides.

A plan has a "rate", which is the amount that a consumer pays as a monthly
premium, in dollars.

A plan has a "rate area", which is a geographic region in a state that
determines the plan's rate. A rate area is a tuple of a state and a number, for
example, NY 1, IL 14.

There are two additional CSV files in this directory besides `slcsp.csv`:

  * `plans.csv` -- all the health plans in the U.S. on the marketplace
  * `zips.csv` -- a mapping of ZIP Code to county/counties & rate area(s)

A ZIP Code can potentially be in more than one county. If the county can not be
determined definitively by the ZIP Code, it may still be possible to determine
the rate area for that ZIP Code.

A ZIP Code can also be in more than one rate area. In that case, the answer is ambiguous
and should be left blank.

This project was developed on and runs with ruby '2.5.1'.

## Setup
$ bundle install

## Run tests
$ rspec

## Runing the slcsp program
From the command line, go to the project directory:
$ ./slcsp --help
Usage: slcsp [options]
        --slcsp=SLCSP                SLCSP CSV file
        --zip=ZIP                    ZIP CSV file
        --plan=PLAN                  Plan CSV file
    -h, --help                       Prints this help

Alternatively, copy the SLCSP, ZIP, and Plan csv files to the
slcsp directory and it will run without command line options.

## Alternative method to launch slcsp program
$ ruby slcsp

## Notes
This project was implemented with a preference for simplicity,
testability, and readability over execution efficiency.
This runs quick enough to not be a hindrance to the person
who needs to run it.

There are further refinements that could be explored like using
a hashmap of tuples to store the plans. A future version may
also place the plans and zips in a database.

