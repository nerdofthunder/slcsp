# frozen_string_literal: true

require 'csv'
require 'pry'

module Model
  class Slcsp
    ZIPCODE = 'zipcode'

    def initialize(slcsp_csv_path)
      @slcsp = slcsp_csv_path
      @csv = CSV.foreach(@slcsp).entries
      headers = @csv.shift
      @zipcode_index = headers.find_index('zipcode')
    end

    def foreach_zip
      @csv.each { |row| yield row[@zipcode_index] }
    end
  end
end
