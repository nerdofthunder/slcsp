# frozen_string_literal: true

require 'csv'
require 'pry'

module Model
  METAL_LEVEL = 'Silver'
  class Plan
    def initialize(plan_csv_path)
      @plans = plan_csv_path

      @csv = CSV.foreach(@plans).entries
      headers = @csv.shift
      @state_index = headers.find_index('state')
      @metal_level_index = headers.find_index('metal_level')
      @rate_index = headers.find_index('rate')
      @rate_area_index = headers.find_index('rate_area')
    end

    def find_and_sort_rates(rate_area)
      @csv.select do |row|
        rate_area[:state] == row[@state_index] &&
          rate_area[:rate_area] == row[@rate_area_index] &&
          METAL_LEVEL == row[@metal_level_index]
      end.map { |row| row[@rate_index].to_f }.sort
    end

    def get_second_lowest_rate(rate_area)
      find_and_sort_rates(rate_area).uniq[1] || ''
    end
  end
end
