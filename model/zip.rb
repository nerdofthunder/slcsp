# frozen_string_literal: true

require 'csv'

module Model
  class Zip
    def initialize(zip_csv_path)
      @zips = zip_csv_path
      @csv = CSV.foreach(@zips).entries
      headers = @csv.shift
      @zipcode_index = headers.find_index('zipcode')
      @state_index = headers.find_index('state')
      @rate_area_index = headers.find_index('rate_area')
    end

    # @return {rate_area, state}
    def search_by_zip(zip)
      # could be more efficient, prefer readability especially
      # at this scale.
      result = @csv.select { |row| row[@zipcode_index] == zip } # find zip codes that match
                   .map { |row| { state: row[@state_index], rate_area: row[@rate_area_index] } } # simplify to info we need
                   .uniq # dedupe any zip codes that are in more than one county
      result.count > 1 ? nil : result[0] # zip codes with multiple rate areas are ambiguous
    end
  end
end
